//
//
// Sample code for reading from HealthKit 
// Authorisation not included, thus has to be implemented separately
// See https://agostini.tech/2019/01/07/using-healthkit/ for further hints
//
func getDistanceCycling() {
    guard let sampleDistanceCycling = HKSampleType.quantityType(forIdentifier: .distanceCycling) else {
        Log.e("Sample type not available")
        return
    }
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd"
    let dateStart = dateFormatter.date(from: "2019-01-01")
    let datePredicate = HKQuery.predicateForSamples(withStart: dateStart, end: Date(), options: .strictEndDate)
    let dcQuery = HKSampleQuery(sampleType: sampleDistanceCycling, predicate: datePredicate, limit: HKObjectQueryNoLimit, sortDescriptors: nil) {
        (query, sample, error) in
        
        guard
            error == nil,
            let quantitySamples = sample as? [HKQuantitySample] else {
                print("Something went wrong: \(error)")
                return
        }
        
        let total = quantitySamples.reduce(0.0) { $0 + $1.quantity.doubleValue(for: HKUnit.meter()) }
        Log.i("Total distance (km): \(total)")
        DispatchQueue.main.async {
            Log.i(String(format: "Total distance (km): %.2f", total))
        }
    }
    HKHealthStore().execute(dcQuery)
}
